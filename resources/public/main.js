var TodoList = React.createClass({

  getInitialState: function() {
    this.fetchTodos();
    return {allTodos: {}};
  },

  fetchTodos: function() {
    var that = this;
    qwest.get('/todos')
     .then(function(xhr, response) {
        that.updateTodos(response);
     });
  },

  updateTodos: function(todos) {
    this.setState({allTodos: todos});
  },

  render: function() {
    var allTodos = this.state.allTodos;
    var todos = [];
    for (var key in allTodos) {
      todos.push(<TodoItem id={key} todo={allTodos[key]} updateList={this.updateTodos}/>);
    }

    return (
      <div>
        <ul id="todo-list">{todos}</ul>
        <AddForm updateList={this.updateTodos}/>
      </div>
    );
  }
});

var AddForm = React.createClass({

  getInitialState: function() {
    return {value: ""};
  },

  onClick: function(evt) {
    var that = this;
    qwest.post('/todos', {label: this.state.value}, {dataType: "json"})
     .then(function(xhr, response) {
        that.setState({value: ""});
        that.props.updateList(response);
     });
  },

  handleChange : function(evt) {
    this.setState({
      value: evt.target.value
    });
  },

  render: function() {
    return (
      <div>
        <input type="text" value={this.state.value} onChange={this.handleChange} />
        <button onClick={this.onClick}>Add a todo</button>
      </div>
    );
  }
});

var TodoItem = React.createClass({

  setDone: function(evt) {
    qwest.post('/todos/done/' + this.props.id)
     .then(function(xhr, response) {
        evt.target.checked = true;
     });
  },

  remove: function(evt) {
    var that = this;
    qwest.delete('/todos/' + this.props.id)
     .then(function(xhr, response) {
        that.props.updateList(response);
     });
  },

  render: function() {
    return <li>{this.props.todo.label} <input type="checkbox" checked={this.props.todo.done} onClick={this.setDone} /> <button onClick={this.remove}>Delete</button></li>;
  }
});

ReactDOM.render(
  <TodoList/>,
  document.getElementById('main')
);
