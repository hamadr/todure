(defproject todure "0.1.0-SNAPSHOT"
  :description "A todo list API"
  ;;:url ""
  ;;:license {:name "Eclipse Public License"
  ;;          :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-jetty-adapter "1.4.0"]
                 [compojure "1.4.0"]
                 [ring/ring-json "0.4.0"]]

  :plugins [[lein-ring "0.8.11"]
            [lein-bower "0.5.1"]]

  :ring {:handler todure.core/app}

  :bower {:directory "resources/public/bower_components"}
  :bower-dependencies [["react" "~0.14.3"]
                       ["babel" "*"]
                       ["qwest" "~2.2.3"]]
  )
