(ns todure.core
  (:use ring.util.response)
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.util.response :refer [response]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-body wrap-json-params]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]))


(def todos (atom {}))

(defn add-todo [label] (swap! todos assoc (inc (key (apply max-key key (assoc @todos 0 {})))) {:label label :done false}))

(defn delete-todo [id] (swap! todos dissoc id))

(defn set-done [id] (swap! todos update-in [id :done] #(or true %)))

(defn get-label [request] (or (get-in request [:params :label]) (get-in request [:body :label]) "null"))

(defroutes app-routes
  (route/resources "/")
  (GET "/" [] (resource-response "index.html" {:root "public"}))
  (GET "/todos" [] (response @todos))
  (POST "/todos" request (response (add-todo (get-label request))))
  (POST "/todos/done/:id" [id] (response (set-done (read-string id))))
  (DELETE "/todos/:id" [id] (response (delete-todo (read-string id))))
  (route/not-found "Invalid route."))

(def app
  (-> (handler/site app-routes)
      wrap-json-response
      wrap-keyword-params
      wrap-json-params))
